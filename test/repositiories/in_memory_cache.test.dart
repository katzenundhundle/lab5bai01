import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import '../../lib/repositories/repository.dart';
import '../../lib/repositories/in_memory_cache.dart';

void main() {
  Repository repository = InMemoryCache();
  test('Test In Memory Cache repository', () {
    expect(repository.getAll().isEmpty, true);

    Model newModel = repository.create();
    expect(newModel.id, 1);

    expect(repository.getAll().isEmpty, false);
    Model? existedModel = repository.get(1);

    expect(newModel, existedModel);

    Model newModel2 = repository.create();

    expect(repository.getAll().length, 2);
  });
  test('Test Model', () {
    Model newModel =
        Model(id: 3, data: {'task_name': 'Finish all assignments'});
    expect(newModel.id, 3);

    expect(newModel.data.isEmpty, false);

    expect(newModel.data['task_name'], 'Finish all assignments');
  });
}

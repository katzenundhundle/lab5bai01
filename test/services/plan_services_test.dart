import 'package:flutter_test/flutter_test.dart';
import '../../lib/models/plan.dart';
import '../../lib/services/plan_services.dart';

void main() {
  test('Test create plan', () {
    PlanServices planServices = PlanServices();

    planServices.createPlan('School');

    var allPlans = planServices.getAllPlans();
    expect(1, allPlans.length);
  });
  test('Test create task by plan services', () {
    PlanServices planServices = PlanServices();

    var plan = Plan(id: 1);
    var description = 'homework';
    planServices.addTask(plan, description);
    print(planServices.getAllPlans()[0].tasks.length);
    expect('homework', planServices.getAllPlans());
  });
}
